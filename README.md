#**Characteristics Of Semiconductor Devices:**

# BC107PLOThFEvsIC
BC107-characteristic  hFE vs IC
!['BC107-characteristic  hFE vs IC'](https://raw.githubusercontent.com/stackprogramer/BC107PLOThFEvsIC/master/images/BC107.png)
BC107-characteristic  hFE vs IC in matlab 
!['BC107-characteristic  hFE vs IC'](https://raw.githubusercontent.com/stackprogramer/BC107PLOThFEvsIC/master/images/BC107-2.png)





##Plot current-voltage  characteristics
Plot current-voltage Drain formula for approximately and accurately.
s.yang Microelectronic Device page 245 equation 9-36&9-37
!['current-voltage characteristics'](https://raw.githubusercontent.com/stackprogramer/BC107PLOThFEvsIC/master/images/ID.png)
